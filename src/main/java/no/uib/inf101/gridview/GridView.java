package no.uib.inf101.gridview;

import javax.swing.JPanel;

import no.uib.inf101.colorgrid.ColorGrid;
import no.uib.inf101.colorgrid.IColorGrid;

import java.awt.Dimension;

import java.awt.Color;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.Rectangle2D;
import no.uib.inf101.colorgrid.CellColorCollection;
import java.util.List;
import no.uib.inf101.colorgrid.CellColor;

public class GridView extends JPanel{
  private IColorGrid p1;
  public GridView(IColorGrid p1) {
    this.setPreferredSize(new Dimension(400, 300));
    this.p1 = p1;
  }

  @Override
  public void paintComponent(Graphics g) {
    super.paintComponent(g);
    Graphics2D g2 = (Graphics2D) g;

    drawGrid(g2);
    getBoundsForCell();

  }
  public void getBoundsForCell() {

  }

  public void drawGrid(Graphics2D g2) {
    double OUTERMARGIN = 30;
    double x = OUTERMARGIN;
    double y = OUTERMARGIN;
    double width = this.getWidth() - 2 * OUTERMARGIN;
    double height = this.getHeight() - 2 * OUTERMARGIN;

    Rectangle2D rectangle = new Rectangle2D.Double(x,y,width, height);
    Color color = Color.LIGHT_GRAY;
    g2.setColor(color);
    g2.fill(rectangle);

    CellPositionToPixelConverter cptpc = new CellPositionToPixelConverter(rectangle, p1 , OUTERMARGIN);
    drawCells(g2,p1, cptpc);
    


  }

  private static void drawCells(Graphics2D canvas, CellColorCollection cells, CellPositionToPixelConverter cptp ) {
    List<CellColor> list = cells.getCells();
    for(CellColor cell:list) {
      Rectangle2D rectangle2d = cptp.getBoundsForCell(cell.cellPosition());
      Color color = cell.color();
      if(cell.color()==null) {
        color = Color.DARK_GRAY;
      }
      canvas.setColor(color);
      canvas.fill(rectangle2d);

    }

  }




}
