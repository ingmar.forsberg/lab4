package no.uib.inf101.gridview;
import javax.swing.JPanel;

import no.uib.inf101.colorgrid.IColorGrid;

import java.awt.Dimension;

import java.awt.Color;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.Rectangle2D;

import no.uib.inf101.colorgrid.CellPosition;
import no.uib.inf101.colorgrid.GridDimension;

public class CellPositionToPixelConverter {
  private Rectangle2D box;
  private GridDimension gd;
  private double margin;

  public CellPositionToPixelConverter(Rectangle2D box, GridDimension gd, double margin) {
    this.box = box;
    this.gd = gd;
    this.margin = margin;
  }

  public Rectangle2D getBoundsForCell(CellPosition cp) {
    double cellWidth = (box.getWidth() - margin * (gd.cols() + 1)) / gd.cols();
    double cellHeight = (box.getHeight() - margin  * (gd.rows() + 1)) / gd.rows();
    double cellX = box.getX() + margin + margin*cp.col() + cellWidth*cp.col();
    double cellY = box.getY() + margin + margin*cp.row() + cellHeight*cp.row();
    return new Rectangle2D.Double(cellX, cellY, cellWidth,  cellHeight);
  }
}
