package no.uib.inf101.colorgrid;

import java.awt.Color;
import java.util.List;
import java.util.ArrayList;



import java.awt.Color;
import java.util.ArrayList;
import java.util.List;

public class ColorGrid implements IColorGrid {

  private int rows;
  private int cols;
  private ArrayList<CellColor> cells;
  

  public ColorGrid(int rows, int cols) {
    this.rows = rows;
    this.cols = cols;
    this.cells = new ArrayList<>();

    for(int i=0; i<rows; i++) {
      for(int j=0; j<cols; j++) {
        cells.add(new CellColor(new CellPosition(i, j), null));
      }
    }
    
  }

  @Override
  public int rows() {
    return rows;
  }

  @Override
  public int cols() {
    return cols;
  }

  @Override
  public List<CellColor> getCells(){
    return cells;
  }
    
  

  @Override
  public Color get(CellPosition pos) {
    if (pos.row() < 0 || pos.row() >= rows || pos.col() < 0 || pos.col() >= cols) {
      throw new IndexOutOfBoundsException("Position is out of bounds.");
    }
    for (CellColor cellColor : cells) {
      if (cellColor.cellPosition().equals(pos)) {
        return cellColor.color();
      }
    }
    return null;
    
  }

  
  @Override
  public void set(CellPosition pos, Color color) {
    if (pos.row() < 0 || pos.row() >= rows || pos.col() < 0 || pos.col() >= cols) {
      throw new IndexOutOfBoundsException("Position is out of bounds.");
    }

    boolean found = false;

    for(int i = 0; i < cells.size(); i++) {
      CellColor cellColor = cells.get(i);
      if(cellColor.cellPosition().equals(pos)) {
        cells.set(i, new CellColor(pos, color));
        found = true;
      }
    }
    if(found==false) {
      cells.add(new CellColor(pos, color));
    }
    
  } 

}



